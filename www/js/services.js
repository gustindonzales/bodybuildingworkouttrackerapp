angular.module('starter.services', [])

.factory('AuthenticationService', ['$http', /*'$cookieStore',*/ '$rootScope', '$window', '$timeout'/*, 'UserService'*/, function($http, /*$cookieStore,*/ $rootScope, $window, $timeout/*, UserService*/){


      var service = {};

      service.Login = Login;
      service.Logout = Logout;
      service.SetCredentials = SetCredentials;
      service.ClearCredentials = ClearCredentials;
      service.GetToken = GetToken;

      return service;

      function GetToken(){
        return $window.localStorage.getItem('accessToken');
      }

      function Login(username, password, callback) {

          /* Dummy authentication for testing, uses $timeout to simulate api call
           ----------------------------------------------*/
          //$timeout(function () {
          //    var response;
          //    UserService.GetByUsername(username)
          //        .then(function (user) {
          //            if (user !== null && user.password === password) {
          //                response = { success: true };
          //            } else {
          //                response = { success: false, message: 'Username or password is incorrect' };
          //            }
          //            callback(response);
          //        });
          //}, 1000);

          /* Use this for real authentication
           ----------------------------------------------*/
          $http.post('https://vps51810.vps.ovh.ca/bbwt/auth/login', { username: username, password: password })
              .success(function (response, status, headers) {
                  if(response.success){
                    SetCredentials(headers()['x-access-token']);
                  }
                  callback(response);
              });

      }

      function Logout(callback){
        ClearCredentials();
        callback({success: true, msg: 'ok'});
      }

      function SetCredentials(accessToken) {
        $window.localStorage.setItem('accessToken', accessToken);
      }

      function ClearCredentials() {
          $window.localStorage.removeItem('accessToken');
      }


}])

.factory('UserService', function($http){
  var service = {};

  service.GetAll = GetAll;
  service.GetById = GetById;
  service.GetByUsername = GetByUsername;
  service.Create = Create;
  service.Update = Update;
  service.Delete = Delete;

  return service;

  function GetAll() {
      return $http.get('/api/users').then(handleSuccess, handleError('Error getting all users'));
  }

  function GetById(id) {
      return $http.get('/api/users/' + id).then(handleSuccess, handleError('Error getting user by id'));
  }

  function GetByUsername(username) {
      return $http.get('/api/users/' + username).then(handleSuccess, handleError('Error getting user by username'));
  }

  function Create(user) {
      return $http.post('https://vps51810.vps.ovh.ca/bbwt/auth/register', user).then(handleSuccess, handleError('Error creating user'));
  }

  function Update(user) {
      return $http.put('/api/users/' + user.id, user).then(handleSuccess, handleError('Error updating user'));
  }

  function Delete(id) {
      return $http.delete('/api/users/' + id).then(handleSuccess, handleError('Error deleting user'));
  }

  // private functions

  function handleSuccess(res) {
      return res.data;
  }

  function handleError(error) {
      return function () {
          return { success: false, message: error };
      };
  }
})

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
