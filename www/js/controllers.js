angular.module('starter.controllers', [])

.controller('LoginCtrl', function($scope, $ionicModal, $window, $http, $log, $location, $state, AuthenticationService, UserService, $ionicPopup){
  $scope.user = {username: '', password: ''};
$scope.isAndroid = ionic.Platform.isAndroid();
  $scope.login = function(form){

    if(form.$valid){
        $scope.dataLoading = true;
          AuthenticationService.Login($scope.user.username, $scope.user.password, function (response) {

              if (response.success) {
                  //$location.path('/dashboard/log');
                  $state.go('tab.log');
                  $scope.dataLoading = false;
              } else {
                $scope.dataLoading = false;
                $scope.user.password = '';
                var message = $ionicPopup.alert({
                   title: 'Invalid Login',
                   template: response.msg
                 });

                 message.then(function(res) {

                });
              }
          });
    }
  };

  $scope.usernameCheck = function(registerForm){
    if($scope.user.username !== ""){
      $http({
        method: "GET",
        url: 'https://vps51810.vps.ovh.ca/bbwt/auth/register/username/' + $scope.user.username
      }).then(function(response){
        if(response.data === true){ // username exists
          var e = $window.document.getElementById('username');
          registerForm.username.$error.unique = true;
          $scope.user.username = '';
          e.focus();
        } else { // username don't exist
          registerForm.username.$error.unique = false;
        }
      }, function(err){
        console.log(err);
      });
    }
  };

  $scope.register = function(registerForm, user){
    if(registerForm.$valid){
      $scope.dataLoading = true;
      UserService.Create($scope.user)
          .then(function (response) {
            console.log(response);
            $scope.message = response.msg;
            $scope.code = response.code;
            $scope.hasMessage = true;
              if (response.success) { // successfull registration
                $scope.success = true;
                $scope.type = 'success';
                //var counter = 1000;
                //$timeout(function(){
                  AuthenticationService.Login($scope.user.username, $scope.user.password, function(response){
                    if (response.success) {
                        $state.go('tab.log');
                        $scope.closeRegisterModal();

                    } else {
                        $scope.type = 'error';
                        $scope.hasMessage = true;
                        $scope.message = 'The login failed. please try to log in manually';
                    }

                    $scope.dataLoading = false;
                  });
                //}, counter);

              } else { // unsuccessfull registration';
                  $scope.message = response.msg;
                  $scope.hasMessage = true;
                  $scope.dataLoading = false;

                  var usernameCodes = [1,2];
                  var emailCodes = [3];
                  if(usernameCodes.indexOf($scope.code) > -1){ // bad username
                    var element = $window.document.getElementById("username");
                    element.focus();
                    $scope.user.username = '';
                  } else if(emailCodes.indexOf($scope.code) > -1){ // bad email
                    var element = $window.document.getElementById("email");
                    element.focus();
                    $scope.user.email = '';
                  }
              }
          });
    }
  };

  $scope.forgotPasswordModal = function(){
    var message = $ionicPopup.alert({
       title: 'Forgot Password',
       template: 'Here the user would be instructed to input their email address. The user would be emailed a link with unique short lived token which they would click to reset their password. This is not implimented because it is not a necessity at the moment'
     });

     message.then(function(res) {

    });
  };

  $ionicModal.fromTemplateUrl('register-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.registerModal = modal;
    //$scope.openRegisterModal();
  });
  $scope.openRegisterModal = function() {
    $scope.user = {};
    $scope.registerModal.show();
  };
  $scope.closeRegisterModal = function() {
    $scope.registerModal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.registerModal.remove();
  });

})

.controller('LogCtrl', function($scope, $http, $log, $ionicModal, $ionicPopup, $window, ionicTimePicker, $ionicListDelegate, AuthenticationService) {
  'use strict';
  $scope.dataLoading = false;
  var token = AuthenticationService.GetToken();

  $scope.showExerciseDelete = false;

  $scope.toggleExerciseDelete = function(){
    $scope.showExerciseDelete = !$scope.showExerciseDelete;
  };

  $scope.showSetDelete = false;

  $scope.toggleSetDelete = function(){
    $scope.showSetDelete = !$scope.showSetDelete;
  };

  $scope.openTimePicker = function(field){

    var timeOpts = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        } else {
          var selectedTime = new Date(val * 1000);
          //console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');

          if(selectedTime.getUTCHours() < 12 ){
            var a = 'AM';
          } else {
            var a = 'PM';
          }

          if(selectedTime.getUTCMinutes() === 0){
            var m = '';
          } else {
            var m = ':' + selectedTime.getUTCMinutes();
          }

          if(field === 'start'){
            $scope.selectedWorkout.startTime = selectedTime;
            $scope.selectedWorkout.startTimeText = selectedTime.getUTCHours() +  m + ' ' + a;
          } else {
            $scope.selectedWorkout.endTime = selectedTime;
            $scope.selectedWorkout.endTimeText = selectedTime.getUTCHours() +  m + ' ' + a;
          }
        }
      },
      //inputTime: 50400,   //Optional
      format: 12,         //Optional
      step: 1,           //Optional
      setLabel: 'Set'    //Optional
    };
    ionicTimePicker.openTimePicker(timeOpts);
  };

  $scope.calendar = {};
  $scope.calendar.eventSource = [];
  $scope.changeMode = function (mode) {
      $scope.calendar.mode = mode;
  };


  $scope.saveWorkout = function (form) {
    if(form.$valid) {
      $scope.dataLoading = true;
      var token = $window.localStorage.getItem('accessToken');


      var w = {
        name: $scope.selectedWorkout.name,
        startDate: moment.utc($scope.selectedWorkout.startDate).toDate(),
        endDate: moment.utc($scope.selectedWorkout.endDate).toDate(),
        color : "#3a87ad"
      };

      $http({
        method: 'POST',
        url: 'https://vps51810.vps.ovh.ca/bbwt/workout',
        data: { accessToken: token, workout: w },
        cache: false
      }).then(function(response){
        var e = {
          id : response.data._id,
          title: response.data.name,
          startTime: moment(response.data.startDate).toDate(),
          endTime: moment(response.data.endDate).toDate(),
          allDay: false
        };

        $scope.calendar.eventSource.push(e);
        $scope.$broadcast('eventSourceChanged', $scope.calendar.eventSource);

        $scope.selectedWorkout = {};
        $scope.closeNewWorkoutModal();
        $scope.dataLoading = false;
      }, function(err){
        console.log(err);
        $scope.dataLoading = false;
      });
    }
  };

  $scope.saveExistingWorkout = function (form, id) {
    if(form.$valid) {
      $scope.dataLoading = true;
      var token = AuthenticationService.GetToken();
      var workout = {
        name: $scope.selectedWorkout.name,
        startDate: $scope.selectedWorkout.startDate,
        endDate: $scope.selectedWorkout.endDate,
        calendarEventColor: $scope.selectedWorkout.color
      };

      var exercises = $scope.selectedWorkout.exercises;

      $http({
        method: 'PUT',
        url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id,
        data: {accessToken: token, workout: workout, exercises: exercises }
      }).then(function(response){

        for(var i = 0; i < $scope.calendar.eventSource.length; i++){
          if($scope.calendar.eventSource[i].id === response.data._id){
            $scope.calendar.eventSource[i] = {
               id: response.data._id,
               startTime: response.data.startDate,
               endTime: response.data.endDate,
               title: response.data.name,
               color: response.data.calendarEventColor,
               allDay: false
             };
             $scope.$broadcast('eventSourceChanged', $scope.calendar.eventSource);
            break;
          }
        }



        $scope.closeExistingWorkoutModal();
        $scope.dataLoading = false;
      }, function(){
        console.log(err);
        $scope.dataLoading = false;
      });
    }
  };

  $scope.deleteWorkout = function (id) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirm Removal',
      template: 'Are you sure you want to remove this Workout?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        $scope.dataLoading = true;
        var token = AuthenticationService.GetToken();

        $http({
          method: 'DELETE',
          url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id + '/?accessToken=' + token
        }).then(function(response){


          for(var i = 0; i < $scope.calendar.eventSource.length; i++){
            if($scope.calendar.eventSource[i].id === id){ // remove event from calendar
              $scope.calendar.eventSource.splice(i , 1);
              $scope.$broadcast('eventSourceChanged', $scope.calendar.eventSource);
              break;
            }
          }

          $scope.closeExistingWorkoutModal();
          $scope.dataLoading = false;
        }, function(err){
          console.log(err);
          $scope.dataLoading = false;
        });
      }
    });
  };

  $scope.saveExercise = function (form, id, eId) {
    if(form.$valid){
      $scope.dataLoading = true;
      var token = $window.localStorage.getItem('accessToken');

      for(var i = 0; i < $scope.selectedWorkout.exercises.length; i++){
        if($scope.selectedWorkout.exercises[i]._id === $scope.exerciseId){
          $scope.exerciseKey = i;
          break;
        }
      }

      var e = {
        name: $scope.selectedWorkout.exercises[$scope.exerciseKey].name,
        note: $scope.selectedWorkout.exercises[$scope.exerciseKey].note
      };

      $http({
        method: 'PUT',
        url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id + '/exercise/' + eId,
        data: { accessToken: token, exercise: e }
      }).then(function(response){
        $scope.closeExerciseModal();
        $ionicListDelegate.closeOptionButtons();
        $scope.dataLoading = false;
      }, function(err){
        console.log(err);
        $scope.dataLoading = false;
      });
    }
  };

  $scope.saveSet = function (form, id, eId, sId) {
    if(form.$valid) {
      $scope.dataLoading = true;
      var token = $window.localStorage.getItem('accessToken');



      var s = {
        reps: $scope.selectedWorkout.exercises[$scope.exerciseKey].sets[$scope.setKey].reps,
        weight: $scope.selectedWorkout.exercises[$scope.exerciseKey].sets[$scope.setKey].weight,
        note: $scope.selectedWorkout.exercises[$scope.exerciseKey].sets[$scope.setKey].note
      };


      $http({
        method: 'PUT',
        url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id + '/exercise/' + eId + '/set/' + sId,
        data: { accessToken: token, set: s }
      }).then(function(response){

        $scope.closeSetModal();
        $ionicListDelegate.closeOptionButtons();
        $scope.dataLoading = false;
      }, function(err){
        console.log(err);
        $scope.dataLoading = false;
      });
    }
  };

  $scope.addExercise = function(id){
    $scope.dataLoading = true;
    var token = AuthenticationService.GetToken();

    $http({
      method: 'POST',
      url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id + '/exercise',
      data: { accessToken: token }
    }).then(function(response){

      var exercise = {
        _id: response.data._id,
        name: response.data.name,
        note: response.data.note,
        sets: response.data.sets
      };

      $scope.selectedWorkout.exercises.push(exercise);
      $scope.dataLoading = false;

    },function(err){
      console.log(err);
      $scope.dataLoading = false;
    });
  };

  $scope.deleteExercise = function (id, eId) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirm Removal',
      template: 'Are you sure you want to remove this exercise?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        $scope.dataLoading = true;
        var token = AuthenticationService.GetToken();

        $http({
          method: 'DELETE',
          url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id + '/exercise/' + eId + '/?accessToken=' + token
        }).then(function(response){


          for(var i = 0; i < $scope.selectedWorkout.exercises.length; i++){
            if($scope.selectedWorkout.exercises[i]._id === response.data._id){ // remove event from calendar
              $scope.selectedWorkout.exercises.splice(i , 1);
              break;
            }
          }


          $scope.showExerciseDelete = false;
          $scope.dataLoading = false;
        }, function(err){
          console.log(err);
          $scope.dataLoading = false;
        });
      }
    });
  };

  $scope.editExerciseModal = function(id, eId){
    var token = AuthenticationService.GetToken();


    $scope.exerciseId = eId;

    for(var i = 0; i < $scope.selectedWorkout.exercises.length; i++){
      if($scope.selectedWorkout.exercises[i]._id === $scope.exerciseId){
        $scope.exerciseKey = i;
        break;
      }
    }

    $ionicModal.fromTemplateUrl('exercise-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.exerciseModal = modal;
      $scope.openExerciseModal();
    });
    $scope.openExerciseModal = function() {
      $scope.exerciseModal.show();
    };
    $scope.closeExerciseModal = function() {
      $scope.exerciseModal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.exerciseModal.remove();
    });

  };

  $scope.addSet = function(id, eId){
    $scope.dataLoading = true;
    var token = AuthenticationService.GetToken();

    $http({
        method: 'POST',
        url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id + '/exercise/' + eId + '/set/?accessToken=' + token
    }).then(function(response){
      var set = {
        _id: response.data._id,
        reps: response.data.reps,
        weight: response.data.weight,
        note: response.data.note
      };

      var exerciseKey = 0;

      for(var i = 0; i < $scope.selectedWorkout.exercises.length; i++){
        if($scope.selectedWorkout.exercises[i]._id === eId){
          exerciseKey = i;
          break;
        }
      }

      $scope.selectedWorkout.exercises[exerciseKey].sets.push(set);
      $scope.selectedWorkout.exercises[exerciseKey].setQuantity = $scope.selectedWorkout.exercises[exerciseKey].sets.length;
      $scope.dataLoading = false;
    },function(err){
      console.log(err);
      $scope.dataLoading = false;
    });
  };

  $scope.deleteSet = function(id, eId, sId){
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirm Removal',
      template: 'Are you sure you want to remove this set?'
    });

    confirmPopup.then(function(res) {
      if(res) {
        $scope.dataLoading = true;
        var token = AuthenticationService.GetToken();

        $http({
          method: 'DELETE',
          url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + id + '/exercise/' + eId + '/set/' + sId +'/?accessToken=' + token
        }).then(function(response){


          for(var i = 0; i < $scope.selectedWorkout.exercises.length; i++){
            if($scope.selectedWorkout.exercises[i]._id === response.data.exercise){ // remove event from calendar
              for(var i2 = 0; i2 < $scope.selectedWorkout.exercises[i].sets.length; i2++){
                if($scope.selectedWorkout.exercises[i].sets[i2]._id === response.data._id){ // remove event from calendar
                  $scope.selectedWorkout.exercises[i].sets.splice(i2 , 1)
                  $scope.selectedWorkout.exercises[i].setQuantity = $scope.selectedWorkout.exercises[i].sets.length;
                  break;
                }
              }
            }

          }

          $scope.showSetDelete = false;
          $scope.dataLoading = false;
        }, function(err){
          console.log(err);
          $scope.dataLoading = false;
        });
      }
    });
  };

  $scope.editSetModal = function(id, eId, sId){
    var token = AuthenticationService.GetToken();

    $scope.workoutId = id;
    $scope.exerciseId = eId;
    $scope.setId = sId;


    for(var i = 0; i < $scope.selectedWorkout.exercises.length; i++){
      if($scope.selectedWorkout.exercises[i]._id === eId){
        $scope.exerciseKey = i;
        for(var i2 = 0; i2 < $scope.selectedWorkout.exercises[i].sets.length; i2++){
          if($scope.selectedWorkout.exercises[i].sets[i2]._id === sId){
            $scope.setKey = i2;
            break;
          }
        }

        break;
      }
    }


    $ionicModal.fromTemplateUrl('set-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.setModal = modal;
      $scope.openSetModal();
    });
    $scope.openSetModal = function() {
      $scope.setModal.show();
    };
    $scope.closeSetModal = function() {
      $scope.setModal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.setModal.remove();
    });

  };

  //get initial event data
  $scope.dataLoading = true;
  $http({
    method: 'GET',
    url: 'https://vps51810.vps.ovh.ca/bbwt/workout/?accessToken=' + token,
  }).then(function(response){
    var events = [];
    for(var i = 0; i < response.data.length; i++){
      var startTime = new Date(response.data[i].startDate);
      var endTime = new Date(response.data[i].endDate);
      var title = response.data[i].name;
      var id = response.data[i]._id;

      events.push({
          id : id,
          title: title,
          startTime: startTime,
          endTime: endTime,
          allDay: false
      });
    }
    $scope.calendar.eventSource =  events;
    $scope.dataLoading = false;
  }, function(err){
    console.log(err);
    $scope.dataLoading = false;
  });


  $scope.onEventSelected = function (event) {
      //console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
      $scope.dataLoading = true;
      $http({
        method: 'GET',
        url: 'https://vps51810.vps.ovh.ca/bbwt/workout/' + event.id + '/?accessToken=' + token
      }).then(function(response){

        var workout = {
          id: response.data._id,
          startDate: moment(response.data.startDate).toDate(),
          endDate: moment(response.data.endDate).toDate(),
          startTimeText: moment(response.data.startDate).format('h:mm A'),
          endTimeText: moment(response.data.endDate).format('h:mm A'),
          name: response.data.name,
          color: response.data.calendarEventColor,
          exercises: response.data.exercises
        };

        $scope.selectedWorkout = workout;
        for(var i = 0; i < $scope.selectedWorkout.exercises.length; i++){
            $scope.selectedWorkout.exercises[i].setQuantity = $scope.selectedWorkout.exercises[i].sets.length;
        }


        $ionicModal.fromTemplateUrl('existing-workout-modal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.existingWorkoutmodal = modal;
          $scope.openExistingWorkoutModal();
          $scope.dataLoading = false;
        });
        $scope.openExistingWorkoutModal = function() {
          $scope.existingWorkoutmodal.show();
        };
        $scope.closeExistingWorkoutModal = function() {
          $scope.existingWorkoutmodal.hide();
        };
        // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.existingWorkoutmodal.remove();
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function() {
          // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function() {
          // Execute action
        });
      });

  };

  $scope.onViewTitleChanged = function (title) {
      $scope.viewTitle = title;
  };

  $scope.today = function () {
      $scope.calendar.currentDate = new Date();
  };

  $scope.isToday = function () {
      var today = new Date(),
          currentCalendarDate = new Date($scope.calendar.currentDate);

      today.setHours(0, 0, 0, 0);
      currentCalendarDate.setHours(0, 0, 0, 0);
      return today.getTime() === currentCalendarDate.getTime();
  };

  $scope.onTimeSelected = function (selectedTime, events) {
      //console.log('Selected time: ' + selectedTime + ', hasEvents: ' + (events !== undefined && events.length !== 0));
  };


  $scope.openNewWorkoutModal = function(){
    var token = AuthenticationService.GetToken();
    $scope.selectedWorkout = {};
    var dateNow = new Date();

    $scope.selectedWorkout.startDate = dateNow;
    $scope.selectedWorkout.endDate = moment(dateNow).add(1, 'hour').toDate();

    $scope.selectedWorkout.startTimeText = moment($scope.selectedWorkout.startDate).format('h:mm A');
    $scope.selectedWorkout.endTimeText = moment($scope.selectedWorkout.endDate).format('h:mm A');



    $ionicModal.fromTemplateUrl('new-workout-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.newWorkoutModal = modal;
      $scope.newWorkoutModalOpen();
    });
    $scope.newWorkoutModalOpen = function() {
      $scope.newWorkoutModal.show();
    };
    $scope.closeNewWorkoutModal = function() {
      $scope.newWorkoutModal.hide();
    };
    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.newWorkoutModal.remove();
    });

  };



})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, $http, $state, $window, $ionicModal, AuthenticationService) {
  /*$scope.settings = {
    enableFriends: true
  };*/

  $scope.errorMessage = '';
  $scope.successMessage = '';
  $scope.passwords = {};

  $ionicModal.fromTemplateUrl('change-password-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.changePasswordModal = modal;
    //$scope.openRegisterModal();
  });
  $scope.openChangePasswordModal = function() {
    $scope.changePasswordModal.show();
  };
  $scope.closeChangePasswordModal = function() {
    $scope.errorMessage = '';
    $scope.successMessage = '';
    $scope.passwords = {};
    $scope.changePasswordModal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.changePasswordModal.remove();
  });

  $scope.changePassword = function(form, passwords){
    if(form.$valid){
      $scope.dataLoading = true;
      var token = $window.localStorage.getItem('accessToken');

      $http({
        method: 'PUT',
        url: 'https://vps51810.vps.ovh.ca/bbwt/user/password'
        , data: {accessToken: token, passwords: passwords}
      }).then(function(response){
        console.log();
        if(response.data.success === true){
          $scope.errorMessage = '';
          $scope.successMessage = response.data.msg;
          $scope.passwords = {};


        } else {
          $scope.successMessage = '';
          $scope.errorMessage = response.data.msg;
        }
        $scope.dataLoading = false;


      }, function(err){
        console.log(err);
        $scope.dataLoading = false;
      });
    }
  };

  $scope.logout = function(){
    AuthenticationService.Logout(function(response){
      if(response.success){
        //$location.path('/login');
        $state.go('login');
      }
    });
  };
});
