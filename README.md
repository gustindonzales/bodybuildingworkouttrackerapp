# Bodybuilding Workout Tracker App

The Bodybuilding workout tracker app offers a system to log and track each of your workouts.

### Prerequisites

Make sure the following are installed.  
- [Node.js with NPM][1]  
- Bower `npm install -g bower`  

[1]: https://nodejs.org/en/download/ "Node.js"

### Installing

1. Clone the repo. `git clone https://gustindonzales@bitbucket.org/gustindonzales/bodybuildingworkouttrackerapp.git`
2. cd into the root of the project and run `npm install`

### Starting the lab
2. cd into the root of the project and run `ionic serve --lab`
